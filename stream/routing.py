import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import re_path, path
from django.core.asgi import get_asgi_application
from webcam.consumers import VideoConsumer

# URLs that handle the WebSocket connection are placed here.
websocket_urlpatterns = [
    path('ws/live/', VideoConsumer.as_asgi()),
]
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'stream.settings')

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AuthMiddlewareStack(
            URLRouter(
                websocket_urlpatterns
            )
        ),
    }
)
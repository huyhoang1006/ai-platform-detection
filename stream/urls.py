"""
URL configuration for stream project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from webcam import views as webcam
from webcam import video_views as webcam_video
from webcam import modelai_views as webcam_model
from webcam import device_views as webcam_device

urlpatterns = [
    path('', webcam.index),
    path('login', webcam.index, name="login"),
    path('getCountOfUser', webcam.getCountOfUser, name="getCountOfUser"),
    path('userManage/<str:token>', webcam.userManage, name="userManage"),
    path('create', webcam.CreateUserAPIView, name="create"),
    path('user_signup', webcam.CreateMessageAPIView, name="user_signup"),
    path('update', webcam.UpdateUserAPIView, name="update"),
    path('delete', webcam.DeleteUserAPIView, name="delete"),
    path('authentication', webcam.authenticate_user, name="authentication"),
    path('video/<str:token>', webcam.video, name='video'),
    path('video_feed', webcam.video_feed, name='video_feed'),

    path('create_video', webcam_video.CreateVideoAPIView, name='create_video'),
    path('update_video', webcam_video.UpdateVideoAPIView, name='update_video'),
    path('get_video', webcam_video.GetVideoAPIView, name='get_video'),
    path('delete_video', webcam_video.DeleteVideoAPIView, name='delete_video'),

    path('create_model', webcam_model.CreateModelAPIView, name='create_model'),
    path('update_model', webcam_model.UpdateModelAPIView, name='update_model'),
    path('get_model', webcam_model.GetModelAPIView, name='get_model'),
    path('delete_model', webcam_model.DeleteModelAPIView, name='delete_model'),

    path('create_device', webcam_device.CreateDeviceAPIView, name='create_model'),
    path('update_device', webcam_device.UpdateDeviceAPIView, name='update_model'),
    path('get_device', webcam_device.GetDeviceAPIView, name='get_model'),
    path('delete_device', webcam_device.DeleteDeviceAPIView, name='delete_model'),
]

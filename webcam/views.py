import json
import os.path
import time

import cv2
import jwt
from django.conf import settings
from django.contrib.auth import user_logged_in
from django.core.serializers import serialize
from django.http import StreamingHttpResponse, HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from m3u8 import M3U8, Segment
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.serializers import jwt_payload_handler

from .models import User, Video, Device
from .models import Message
from .serializers import UserSerializer, MessageSerializer, VideoSerializer, DeviceSerializer, ModelAiSerializer
from .customPermission import CustomPermission, CustomPermissionAdmin, CustomPermissionUser

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))


# Create your views here.


def index(request):
    return render(request, 'index.html')


@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user(request):
    try:
        email = request.data['email']
        password = request.data['password']

        try:
            user = User.objects.get(email=email, password=password)
        except Exception as e:
            res = {
                'success': 'false',
                'message': 'User or Password is wrong'
            }
            return Response(res)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)

                user_details = {}
                user_details['name'] = "%s %s" % (
                    user.first_name, user.last_name)
                user_details['first_name'] = user.first_name
                user_details['last_name'] = user.last_name
                user_details['email'] = user.email
                user_details['token'] = token
                user_details['id'] = user.id
                user_details['is_staff'] = user.is_staff
                user_details['is_superuser'] = user.is_superuser
                user_details['success'] = 'true'
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return Response(user_details, status=status.HTTP_200_OK)
            except Exception as e:
                res = {
                    'success': 'false',
                    'message': 'Some error 1'
                }
                return Response(res)
                # raise e
        else:
            res = {
                'success': 'false',
                'message': 'can not authenticate with the given credentials or the account has been deactivated'
            }
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except Exception as e:
        res = {
            'success': 'false',
            'message': 'Some error 2'
        }
        return Response(res)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def CreateUserAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    user = request.data
    try:
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
def CreateMessageAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    message = request.data
    messageData = {
        "request": "create",
        "extend": str(message)
    }
    try:
        serializer = MessageSerializer(data=messageData)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def UpdateUserAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    user = request.data
    email = user.get('email')

    try:
        if user['is_superuser'] == 1:
            user['is_superuser'] = True
        else:
            user['is_superuser'] = False
        user_instance = User.objects.get(email=email)
        serializer = UserSerializer(user_instance, data=user, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def DeleteUserAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    user = request.data
    email = user.get('email')

    try:
        user_instance = User.objects.get(email=email)
        user_instance.delete()
        res["data"] = "User deleted successfully"
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


def userManage(request, token):
    try:
        userProvide = jwt.decode(token, settings.SECRET_KEY)
        now = int(time.time())
        try:
            user = User.objects.get(email=userProvide['email'], id=userProvide['user_id'])
        except Exception as e:
            print(e)
            response = redirect('login')
            return response
        if user:
            if user.is_superuser == 'true' or user.is_superuser == True:
                if now > userProvide['exp']:
                    response = redirect('login')
                    return response
                else:
                    userList = User.objects.all()
                    count = User.objects.count()
                    userListData = serialize('json', userList)
                    messageList = Message.objects.all()
                    messageListData = serialize('json', messageList)
                    return render(request, 'userManage.html',
                                  context={
                                      "userList": userList,
                                      "count": count,
                                      "userListData": userListData,
                                      "messageList": messageList,
                                      "messageListData": messageListData
                                  }
                                  )
            else:
                response = redirect('login')
                return response
    except Exception as e:
        print(e)
        response = redirect('login')
        return response


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getCountOfUser(request):
    count = User.objects.count()
    return Response(count)


def signup(request):
    return render(request, 'signup.html')


def video(request, token):
    try:
        userProvide = jwt.decode(token, settings.SECRET_KEY)
        now = int(time.time())
        try:
            user = User.objects.get(email=userProvide['email'], id=userProvide['user_id'])
        except Exception as e:
            print(e)
            response = redirect('login')
            return response
        if user:
            if user.is_superuser == 'true' or user.is_superuser == True:
                if now > userProvide['exp']:
                    response = redirect('login')
                    return response
                else:
                    video_list = Video.objects.all()
                    device_list = Device.objects.all()
                    return render(request, 'stream.html',
                                  context={
                                      "Role": "Admin",
                                      "video_list": video_list,
                                      "device_list": device_list
                                  }
                                  )
            else:
                if now > userProvide['exp']:
                    response = redirect('login')
                    return response
                else:
                    return render(request, 'stream.html',
                                  context={
                                      "Role": "User"
                                  }
                                  )
    except Exception as e:
        print(e)
        response = redirect('login')
        return response


def stream(video_path):
    cap = cv2.VideoCapture(video_path)
    count = 0
    while True:
        ret, frame = cap.read()

        if not ret:
            break

        _, buffer = cv2.imencode('.jpg', frame)
        frame_data = buffer.tobytes()

        frame_data_bytes = bytes(frame_data)
        count = count + 1
        if count % 2 == 0:
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame_data_bytes + b'\r\n')
    cap.release()


def video_feed(request):
    video_path = 'Video/car.mp4'
    return StreamingHttpResponse(stream(video_path), content_type='multipart/x-mixed-replace; '
                                                                  'boundary=frame')


def generate_hls(request):
    # Đường dẫn đến video
    video_path = 'Video/car.mp4'

    # Tạo một đối tượng M3U8 manifest
    playlist = M3U8()

    # Tạo các segment và thêm vào manifest
    for i in range(0, 10):  # Đây chỉ là ví dụ, bạn có thể tạo segment dựa trên thời gian thực tế của video
        segment = Segment(uri=f'/video/segment_{i}.ts', duration=10.0)
        playlist.add_segment(segment)

    # Trả về manifest như một phản hồi HTTP
    response = HttpResponse(playlist.dumps(), content_type='application/vnd.apple.mpegurl')
    return response

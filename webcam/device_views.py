from django.core.serializers import serialize
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from .customPermission import CustomPermission, CustomPermissionUser, CustomPermissionAdmin
from .serializers import DeviceSerializer
from rest_framework.response import Response
from .models import Device


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def CreateDeviceAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    deviceData = request.data

    try:
        serializer = DeviceSerializer(data=deviceData)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def UpdateDeviceAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    deviceData = request.data

    try:
        device_instance = Device.objects.get(pk=deviceData['id'])
        serializer = DeviceSerializer(device_instance, data=deviceData, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Get'])
@permission_classes([CustomPermissionAdmin])
def GetDeviceAPIView(request):
    res = {
        "success": True,
        "data": ''
    }

    try:
        device_instance = Device.objects.all()
        device_instance_list = serialize('json', device_instance)
        res["data"] = device_instance_list
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def DeleteDeviceAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    deviceData = request.data

    try:
        device_instance = Device.objects.get(pk=deviceData['id'])
        device_instance.delete()
        res["data"] = "Device deleted successfully"
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)
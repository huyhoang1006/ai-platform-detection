from django.core.serializers import serialize
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from .customPermission import CustomPermission, CustomPermissionUser, CustomPermissionAdmin
from .serializers import VideoSerializer
from rest_framework.response import Response
from .models import Video


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def CreateVideoAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    videoData = request.data

    try:
        serializer = VideoSerializer(data=videoData)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def UpdateVideoAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    videoData = request.data

    try:
        video_instance = Video.objects.get(pk=videoData['id'])
        serializer = VideoSerializer(video_instance, data=videoData, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Get'])
@permission_classes([CustomPermissionAdmin])
def GetVideoAPIView(request):
    res = {
        "success": True,
        "data": ''
    }

    try:
        video_instance = Video.objects.all()
        video_instance_list = serialize('json', video_instance)
        res["data"] = video_instance_list
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def DeleteVideoAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    videoData = request.data
    print(videoData)

    try:
        video_instance = Video.objects.get(pk=videoData['id'])
        video_instance.delete()
        res["data"] = "Video deleted successfully"
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)
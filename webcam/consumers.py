import asyncio
import base64
import json

import cv2
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import async_to_sync


class VideoConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_connected = False
        self.video_path = 'Video/car.mp4'  # Cài đặt đường dẫn video ở đây
        self.cap = None
        self.frame_count = 0
        self.sending_frames = False
        self.current_frame = 0  # Lưu vị trí hiện tại của frame
        self.initialize_capture()
        self.fps = 60
        self.sum_frame = 0
        self.batch_frame = 0
        self.command_queue = asyncio.Queue()

    def initialize_capture(self):
        self.cap = cv2.VideoCapture(self.video_path)

    async def connect(self):
        await self.accept()

        self.video_path = 'Video/car.mp4'  # Update with your video file path
        self.cap = cv2.VideoCapture(self.video_path)
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.sum_frame = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.batch_frame = 1
        self.frame_count = 0
        self.sending_frames = False
        self.current_frame = 0  # Lưu vị trí hiện tại của frame
        self.command_queue = asyncio.Queue()
        self.is_connected = True
        asyncio.ensure_future(self.consumer_handler())

    async def start_sending_frames(self):
        self.sending_frames = True
        await self.send_frames()

    async def stop_sending_frames(self):
        self.sending_frames = False
        self.current_frame = self.frame_count  # Lưu vị trí của frame khi dừng

    async def resume_sending_frames(self):
        self.sending_frames = True
        await self.send_frames()

    async def send_frames(self):
        # await self.send(text_data=json.dumps({
        #     'fps': self.fps,
        #     'sum_frame': self.sum_frame
        # }))
        while self.sending_frames == True and self.is_connected == True:
            ret, frame = self.cap.read()
            if not ret:
                # End of video
                break

            self.frame_count += 1

            _, buffer = cv2.imencode('.jpg', frame)
            # frame_data = base64.b64encode(buffer).decode('utf-8')

            frame_data = buffer.tobytes()
            # frame_data_bytes = bytes(frame_data)

            self.current_frame = self.frame_count
            # await self.send(text_data=json.dumps({
            #     'frame': frame_data_bytes
            # }))
            await self.send(bytes_data=frame_data)

            await asyncio.sleep(0.0001)  # Đợi 10 milliseconds trước khi gửi frame tiếp theo

            # break

        if self.sending_frames:
            await self.send(text_data=json.dumps({'complete': True}))

    async def disconnect(self, close_code):
        if self.cap:
            self.cap.release()
        await self.close()

    async def receive(self, text_data):
        command = json.loads(text_data).get('command')

        if command == 'start':
            if not self.sending_frames:
                asyncio.ensure_future(self.start_sending_frames())
        elif command == 'stop':
            if self.sending_frames:
                await self.stop_sending_frames()
        elif command == 'resume':
            if not self.sending_frames:
                asyncio.ensure_future(self.start_sending_frames())
        # elif command == 'disconnect':
        #     if not self.sending_frames:
        #         asyncio.ensure_future(self.disconnect(100))

    async def consumer_handler(self):
        while True:
            command = await self.command_queue.get()
            if command == 'start':
                if not self.sending_frames:
                    await self.start_sending_frames()
            elif command == 'stop':
                if self.sending_frames:
                    await self.stop_sending_frames()
            elif command == 'resume':
                if not self.sending_frames:
                    await self.start_sending_frames()
            # elif command == 'disconnect':
            #     if not self.sending_frames:
            #         asyncio.ensure_future(self.disconnect(100))

const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');
const signInClick = document.getElementById('signInClick')
const warningMessage = document.getElementById('warningMessage')
const loading = document.getElementById('loading')



signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});

let first_name = document.getElementById('first_name');
let last_name = document.getElementById('last_name');
let emailSignUp = document.getElementById('emailSignUp');
let passwordSignUp = document.getElementById('passwordSignUp');
let passwordSignUpRepeat = document.getElementById('passwordSignUpRepeat');

const warningMessageSignUp = document.getElementById('warningMessageSignUp')
const loadingSignUp = document.getElementById('loadingSignUp')
const successMessageSignUp = document.getElementById('successMessageSignUp')


function signUp() {
    if(first_name.value == '' || last_name.value == '' || emailSignUp.value == '' || passwordSignUp.value == '' || passwordSignUpRepeat.value == '' || passwordSignUp.value != passwordSignUpRepeat.value) {
        warningMessageSignUp.innerHTML = 'All of input cannot be null'
        warningMessageSignUp.style.display = 'inline'
        warningMessageSignUp.style.opacity = '1'
        warningMessageSignUp.style.transition = '2s opacity'
        setTimeout(function(){
            warningMessageSignUp.style.opacity = '0'
        }, 2000);
        setTimeout(function(){
            warningMessageSignUp.style.display = 'none'
        }, 3000);
    } else {
        loadingSignUp.style.display = 'inline'
        loadingSignUp.style.border = '2px solid #f3f3f3'
        loadingSignUp.style.borderRadius = '50%'
        loadingSignUp.style.borderTop = '2px solid blue'
        loadingSignUp.style.borderRight = '2px solid green'
        loadingSignUp.style.borderBottom = '2px solid red'
        loadingSignUp.style.borderLeft = '2px solid pink'

        let data = {
            "email": emailSignUp.value,
            "password" : passwordSignUp.value,
            "first_name" : first_name.value,
            "last_name" : last_name.value
        }

        fetch("user_signup", {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(
            response => response.json()
        ).then(response => {
            if(response.success == "true" || response.success == true) {
                loadingSignUp.style.display = 'none'
                successMessageSignUp.style.display = 'inline'
                successMessageSignUp.style.opacity = '1'
                successMessageSignUp.style.transition = '2s opacity'
                setTimeout(function(){
                    successMessageSignUp.style.opacity = '0'
                }, 2000);
                setTimeout(function(){
                    successMessageSignUp.style.display = 'none'
                }, 3000);
            } else {
                warningMessageSignUp.innerHTML = response.message
                warningMessageSignUp.style.display = 'inline'
                warningMessageSignUp.style.opacity = '1'
                warningMessageSignUp.style.transition = '2s opacity'
                setTimeout(function(){
                    warningMessageSignUp.style.opacity = '0'
                }, 2000);
                setTimeout(function(){
                    warningMessageSignUp.style.display = 'none'
                }, 3000);
                loadingSignUp.style.display = 'none'
            }
        })
    }
}

async function validateSignInForm() {
    let x = document.forms["SignInForm"]['emailSignIn'].value;
    let y = document.forms["SignInForm"]["pwSignIn"].value;
    if(x == '') {
        warningMessage.innerHTML = 'Email cannot be null'
        warningMessage.style.display = 'inline'
        warningMessage.style.opacity = '1'
        warningMessage.style.transition = '2s opacity'
        setTimeout(function(){
            warningMessage.style.opacity = '0'
        }, 2000);
        setTimeout(function(){
            warningMessage.style.display = 'none'
        }, 3000);
    } else {
        if(y == '') {
            warningMessage.innerHTML = 'Password cannot be null'
            warningMessage.style.display = 'inline'
            warningMessage.style.opacity = '1'
            warningMessage.style.transition = '2s opacity'
            setTimeout(function(){
                warningMessage.style.opacity = '0'
            }, 2000);
            setTimeout(function(){
                warningMessage.style.display = 'none'
            }, 3000);
        } else {
            loading.style.display = 'inline'
            loading.style.border = '2px solid #f3f3f3'
            loading.style.borderRadius = '50%'
            loading.style.borderTop = '2px solid blue'
            loading.style.borderRight = '2px solid green'
            loading.style.borderBottom = '2px solid red'
            loading.style.borderLeft = '2px solid pink'

            fetch("authentication", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({ "email": x, "password" : y})
            }).then(
                response => response.json()
            ).then(response => {
                if(response.success == "true") {
                    localStorage.setItem("data", JSON.stringify(response));
                    if(response.is_superuser == true || response.is_superuser == 'true') {
                        window.location.href = "userManage/" + response.token
                        loading.style.display = 'none'
                    } else {
                        window.location.href = "video/" + response.token
                        loading.style.display = 'none'
                    }
                } else {
                    warningMessage.innerHTML = response.message
                    warningMessage.style.display = 'inline'
                    warningMessage.style.opacity = '1'
                    warningMessage.style.transition = '2s opacity'
                    setTimeout(function(){
                        warningMessage.style.opacity = '0'
                    }, 2000);
                    setTimeout(function(){
                        warningMessage.style.display = 'none'
                    }, 3000);
                    loading.style.display = 'none'
                }
            })
        }
    }
}

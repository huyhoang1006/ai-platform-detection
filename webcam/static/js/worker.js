let frames = [];

onmessage = function(event) {
    const data = event.data;
    if (data.type === 'addFrame') {
        frames = frames.concat(data.frames);
    } else if (data.type === 'getFrames') {
        postMessage({ type: 'frames', frames: frames });
    }
};
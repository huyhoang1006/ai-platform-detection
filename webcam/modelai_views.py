from django.core.serializers import serialize
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from .customPermission import CustomPermission, CustomPermissionUser, CustomPermissionAdmin
from .serializers import ModelAiSerializer
from rest_framework.response import Response
from .models import ModelAi


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def CreateModelAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    modelAiData = request.data

    try:
        serializer = ModelAiSerializer(data=modelAiData)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def UpdateModelAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    modelAiData = request.data

    try:
        modelai_instance = ModelAi.objects.get(pk=modelAiData['id'])
        serializer = ModelAiSerializer(modelai_instance, data=modelAiData, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        res["data"] = serializer.data
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Get'])
@permission_classes([CustomPermissionAdmin])
def GetModelAPIView(request):
    res = {
        "success": True,
        "data": ''
    }

    try:
        modelAi_instance = ModelAi.objects.all()
        modelAi_instance_list = serialize('json', modelAi_instance)
        res["data"] = modelAi_instance_list
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['Post'])
@permission_classes([CustomPermissionAdmin])
def DeleteModelAPIView(request):
    res = {
        "success": True,
        "data": ''
    }
    # Allow any user (authenticated or not) to access this url
    modelAiData = request.data

    try:
        modelAi_instance = ModelAi.objects.get(pk=modelAiData['id'])
        modelAi_instance.delete()
        res["data"] = "Model AI deleted successfully"
        return Response(res, status=status.HTTP_201_CREATED)
    except Exception as e:
        print(e)
        res["success"] = False
        return Response(res, status=status.HTTP_403_FORBIDDEN)
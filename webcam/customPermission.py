from rest_framework import permissions
from django.core.serializers import serialize


class CustomPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated:
            return True


class CustomPermissionAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated and request.user.is_superuser:
            return True


class CustomPermissionUser(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated and not request.user.is_superuser:
            return True

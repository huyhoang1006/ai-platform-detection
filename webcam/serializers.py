from rest_framework import serializers
from .models import User
from .models import Message, Video, Device, ModelAi


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.ReadOnlyField()

    class Meta(object):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name',
                  'date_joined', 'password', 'is_superuser')
        extra_kwargs = {'password': {'write_only': True}}


class MessageSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Message
        fields = ('id', 'request', 'extend')


class VideoSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Video
        fields = ('id', 'name', 'path', 'info', 'used', 'extend')


class ModelAiSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ModelAi
        fields = ('id', 'name', 'path', 'info', 'used', 'extend')


class DeviceSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Device
        fields = ('id', 'name', 'path', 'info', 'used', 'extend')

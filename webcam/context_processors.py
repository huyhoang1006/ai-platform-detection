from django.conf import settings


def custom_settings(request):
    return {
        'MY_SETTING': settings.MY_SETTING,
        # Thêm bất kỳ setting khác bạn muốn sử dụng trong template
    }
